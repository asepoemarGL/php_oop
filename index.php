<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    echo "<h3>Output</h3>";

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name ."<br>";
    echo "Legs : ".$kodok->legs ."<br>";
    echo "Cold Blooded : ".$kodok->cold_blooded ."<br>";
    echo $kodok->jump("hop hop")."<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : ".$sungokong->name ."<br>";
    echo "Legs : ".$sungokong->legs ."<br>";
    echo "Cold Blooded : ".$sungokong->cold_blooded ."<br>";
    echo $sungokong->yell()."<br><br>";

    // $bis = new Bus("Scania");
    // echo "Nama Mobil : ".$bis->name ."<br>";
    // echo "Jumlah Roda : ".$bis->roda ."<br>";
    // echo "Jenis Fuel : ".$bis->bahanBakar ."<br>";
    // echo "Spion ada ".$bis->spion ."<br>";
    // echo $bis->Jalan();
?>